import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import StoryItem from './StoryItem';
import { fetchItem } from '../services/hackerNews';

function StoryListItem(props) {
	const { className, index, id } = props;
	const [item, setItem] = useState(null);

	useEffect(() => {
		if (id) {
			const fetchData = async () => {
				const newItem = await fetchItem(id);
				setItem(newItem);
			};
			fetchData();
		}
	}, [id]);

	if (!item) {
		return null;
	}

	return (
		<div className={clsx('root', className)}>
			<StoryItem item={item} index={index} />
			{/* language=CSS */}
			<style jsx>{`
				.root {
				}
			`}</style>
		</div>
	);
}

export default StoryListItem;
