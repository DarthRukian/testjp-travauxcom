import React from 'react';
import clsx from 'clsx';
import Link from 'next/link';
import { useRouter } from 'next/router';

const navLinks = [
	{
		label: 'new',
		href: '/',
	},
	/* use for later
	{
		label: 'comments',
		href: '/comments',
	},*/
];

function Nav(props) {
	const { className } = props;
	const router = useRouter();

	const isActiveRoute = route => router.pathname === route;

	return (
		<nav className={clsx('root', className)}>
			<ul className="nav">
				{navLinks.map(({ label, href }) => (
					<li
						key={href}
						className={clsx('nav__item', {
							'is-active': isActiveRoute(href),
						})}
					>
						<Link href={href}>
							<a className="nav__item-link">{label}</a>
						</Link>
					</li>
				))}
			</ul>
			{/* language=CSS */}
			<style jsx>{`
				.root {
				}
				.nav {
					display: flex;
					list-style: none;
					padding: 0;
					margin: 0;
				}
				.nav__item:not(:last-child)::after {
					content: '|';
				}
				.nav__item .nav__item-link {
					color: inherit;
					text-decoration: none;
					margin-left: 4px;
					margin-right: 4px;
				}
				.nav__item.is-active .nav__item-link {
					color: #fff;
					font-weight: bold;
				}
			`}</style>
		</nav>
	);
}

export default Nav;
