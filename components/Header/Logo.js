import React from 'react';
import clsx from 'clsx';

function Logo(props) {
	const { className } = props;
	return (
		<>
			<div className={clsx('root', className)}>JP</div>
			{/* language=CSS */}
			<style jsx>{`
				.root {
					display: inline-flex;
					align-items: center;
					justify-content: center;
					width: 1.5em;
					height: 1.5em;
					background-color: #fe4605;
					border: 1px solid;
					color: #fff;
					font-weight: bold;
					font-size: 16px;
				}
			`}</style>
		</>
	);
}

export default Logo;
