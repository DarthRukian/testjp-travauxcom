import React from 'react';
import clsx from 'clsx';
import { useRouter } from 'next/router';
import Logo from './Logo';
import Nav from './Nav';

function Header(props) {
	const { className } = props;
	const router = useRouter();
	return (
		<>
			<div className={clsx(className, 'root')}>
				<Logo />
				<div className="title">Hacker News</div>
				<Nav className="nav" />
			</div>
			{/* language=CSS */}
			<style jsx>{`
				.root {
					display: flex;
					flex-wrap: wrap;
					align-items: center;
					background-color: #fe4605;
					padding: 5px;
				}
				.title {
					margin-left: 8px;
					margin-right: 8px;
					font-weight: bold;
				}
			`}</style>
		</>
	);
}

export default Header;
