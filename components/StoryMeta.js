import React from 'react';
import clsx from 'clsx';
import TimeAgo from 'react-timeago';
import Link from 'next/link';
import { pluralize } from '../utils';

function StoryMeta(props) {
	const { className, item } = props;
	const { id, by, score, time, descendants } = item;

	return (
		<div className={clsx('root', className)}>
			<span className="score">
				{score} point{pluralize(score)}
			</span>{' '}
			<span className="by">by {by}</span>{' '}
			<span className="time">
				<TimeAgo date={time * 1000} />
			</span>
			{' | '}
			<Link href={`story/${id}`}>
				<a className="action">
					{descendants > 0
						? descendants + ` comment${pluralize(descendants)}`
						: 'discuss'}
				</a>
			</Link>
			{/* language=CSS */}
			<style jsx>{`
				.root {
					color: #666;
					font-size: 0.8em;
				}
			`}</style>
		</div>
	);
}

export default StoryMeta;
