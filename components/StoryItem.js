import React from 'react';
import clsx from 'clsx';
import StoryItemTitle from './StoryItemTitle';
import StoryMeta from './StoryMeta';

function StoryItem(props) {
	const { className, item, index } = props;

	return (
		<div className={clsx('root', className)}>
			{typeof index !== 'undefined' && <div className="rank">{index + 1}</div>}
			<div className="wrapper">
				<StoryItemTitle item={item} />
				<StoryMeta item={item} />
			</div>
			{/* language=CSS */}
			<style jsx>{`
				.root {
					display: flex;
				}
				.rank {
					flex: 0 0 32px;
					text-align: right;
					color: #666;
				}
				.rank::after {
					content: '.';
				}
				.wrapper {
					flex: 1;
					margin-left: 8px;
				}
			`}</style>
		</div>
	);
}

export default StoryItem;
