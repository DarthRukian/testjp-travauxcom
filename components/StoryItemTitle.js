import React from 'react';
import clsx from 'clsx';

function StoryItemTitle(props) {
	const { className, item } = props;
	const { url, title } = item;
	let hostname;

	if (url) {
		hostname = new URL(url).hostname;
	}

	return (
		<div className={clsx('root', className)}>
			{url && (
				<>
					<a href={url} className="title__link">
						{title}
					</a>
					<span className="hostname">({hostname})</span>
				</>
			)}
			{!url && title}
			{/* language=CSS */}
			<style jsx>{`
				.title__link {
				}
				.hostname {
					font-size: 0.8em;
					color: #666;
					margin-left: 4px;
				}
			`}</style>
		</div>
	);
}

export default StoryItemTitle;
