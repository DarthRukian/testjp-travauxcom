import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { fetchNewStories } from '../services/hackerNews';
import StoryListItem from './StoryListItem';

function StoryList(props) {
	const { className } = props;
	const [stories, setStories] = useState([]);

	useEffect(() => {
		const fetchData = async () => {
			const allNewStoriesId = await fetchNewStories();

			// only keep 30 first stories
			const newStoriesId = allNewStoriesId.slice(0, 30);
			setStories(newStoriesId);
		};
		fetchData();
	}, []);

	if (!stories.length) {
		return null;
	}

	return (
		<div className={clsx('root', className)}>
			{stories.map((id, index) => (
				<StoryListItem key={id} id={id} index={index} className="list__item" />
			))}
			{/* language=CSS */}
			<style jsx>{`
				.root {
					padding-top: 16px;
					padding-right: 8px;
				}
				.root :global(.list__item) {
					margin-bottom: 16px;
				}
			`}</style>
		</div>
	);
}

export default StoryList;
