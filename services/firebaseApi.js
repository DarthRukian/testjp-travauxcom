import firebase from 'firebase';
import 'firebase/firestore';

const config = {
	databaseURL: 'https://hacker-news.firebaseio.com',
};

const version = '/v0';

export default !firebase.apps.length
	? firebase.initializeApp(config).database().ref(version)
	: firebase.app().database().ref(version);
