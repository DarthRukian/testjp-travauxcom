import firebaseApi from './firebaseApi';

const itemRef = id => {
	return firebaseApi.child('item/' + id);
};

const newstoriesRef = () => {
	return firebaseApi.child('newstories');
};

export const fetchItem = async id => {
	return await itemRef(id)
		.once('value')
		.then(snapshot => snapshot.val());
};

export const fetchNewStories = async () => {
	return await newstoriesRef()
		.once('value')
		.then(snapshot => snapshot.val());
};
