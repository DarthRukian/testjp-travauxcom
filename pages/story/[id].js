import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { useRouter } from 'next/router';
import StoryItem from '../../components/StoryItem';
import { fetchItem } from '../../services/hackerNews';

function Story(props) {
	const { className } = props;
	const router = useRouter();
	const { id } = router.query;
	const [item, setItem] = useState(null);

	useEffect(() => {
		if (id) {
			const fetchData = async () => {
				const newItem = await fetchItem(id);
				setItem(newItem);
			};
			fetchData();
		}
	}, [id]);

	if (!item) {
		return <div>Loading...</div>;
	}

	return (
		<div className={clsx('root', className)}>
			<StoryItem item={item} />
			{/* language=CSS */}
			<style jsx>{`
				.root {
					padding: 16px;
				}
			`}</style>
		</div>
	);
}

export default Story;
