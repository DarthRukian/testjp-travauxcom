import React from 'react';
import Head from 'next/head';
import Header from '../components/Header/Header';

function MyApp(props) {
	const { Component, pageProps } = props;

	return (
		<>
			<Head>
				<title>Testjp TravauxCom</title>
				<meta charSet="utf-8" />
				<link rel="icon" href="/favicon.ico" />
			</Head>
			<div className="root">
				<Header />
				<div className="content">
					<Component {...pageProps} />
				</div>
			</div>
			{/* language=CSS */}
			<style jsx>{`
				.root {
					max-width: 1280px;
					background-color: #f5f5f5;
					color: #000;
				}

				@media only screen and (min-width: 980px) {
					.root {
						margin: 8px auto;
						width: 90%;
					}
				}
			`}</style>
			{/* language=CSS */}
			<style jsx global>{`
				html,
				body {
					padding: 0;
					margin: 0;
					font-family: Roboto, sans-serif;
					font-size: 16px;
					line-height: 1.5;
				}

				html {
					box-sizing: border-box;
				}

				*,
				*:before,
				*:after {
					box-sizing: inherit;
				}

				a {
					color: inherit;
					text-decoration: none;
				}
				a:hover {
					text-decoration: underline;
				}
			`}</style>
		</>
	);
}

export default MyApp;
