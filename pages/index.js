import React from 'react';
import StoryList from '../components/StoryList';

const Home = () => (
	<div className="root">
		<StoryList />
	</div>
);

export default Home;
